%global srcname cython
%bcond_with test

Name:           Cython
Version:        3.0.11
Release:        1
Summary:        Language for writing C extensions for Python
License:        Apache 2.0
URL:            https://cython.org/
Source0:        %{pypi_source cython}

BuildRequires:  gcc
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  hostname
BuildRequires:  bc
BuildRequires:  diffutils

%if %{with test}
BuildRequires:  gcc-c++
BuildRequires:  python3-coverage
BuildRequires:  python3-numpy
BuildRequires:  python3-jedi
%endif

%global _description \
Cython is a language that makes writing C extensions\
for Python as easy as Python itself.

%description %{_description}

%package -n python3-%{name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{name}}

%description -n python3-%{name} %{_description}

Python 3 version.

%prep
%autosetup -n cython-%{version} -p1

%build
%py3_build

%install
%py3_install
rm -rf %{buildroot}%{python3_sitelib}/setuptools/tests

%if %{with test}
%check
%{__python3} runtests.py -vv
%endif

%files -n python3-%{name}
%license LICENSE.txt COPYING.txt
%doc CHANGES.rst README.rst
%{_bindir}/cython
%{_bindir}/cygdb
%{_bindir}/cythonize
%{python3_sitearch}/%{name}-*.egg-info/
%{python3_sitearch}/%{name}/
%{python3_sitearch}/pyximport/
%{python3_sitearch}/%{srcname}.py
%{python3_sitearch}/__pycache__/%{srcname}.*


%changelog
* Tue Sep 10 2024 Ge Wang <wang__ge@126.com> - 3.0.11-1
- Update version t0 3.0.11

* Tue Jan 30 2024 xu_ping <707078654@qq.com> - 3.0.8-1
- update version to 3.0.8

* Mon Jul 10 2023 chenzixuan <chenzixuan@kylinos.cn> - 0.29.36-1
- update version to 0.29.36

* Mon Dec 13 2021 shixuantong <shixuantong@huawei.com> - 0.29.25-1
- update version to 0.29.25

* Fri Aug 13 2021 liksh <liks11@chinaunicom.cn> - 0.29.21-1
- update to 0.29.21

* Fri Oct 30 2020 zhangtao <zhangtao221@huawei.com> - 0.29.14-4
- remove python2

* Fri Sep 11 2020 liuweibo <liuweibo10@huawei.com> - 0.29.14-3
- Fix Source0

* Tue Nov 5 2019 shanshishi <shanshishi@huawei.com> - 0.29.14-1
- Init package

